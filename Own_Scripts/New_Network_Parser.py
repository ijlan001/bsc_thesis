#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

A script that creates a tab-delimited dissociation matrix. First it
creates a list of BGCs and a value dictionary of the data in the
inputfile. Then, it creates an NDArray from the retrieved data. Lastly,
it writes a text file containing the corresponding matrix. Later on,
this matrix can easily be imported in R for analyses and visualization.
"""
from sys import argv
import os.path
import time
import numpy as np

def Get_Data(InFileName, Modus):
    """
    Parses the input-file and searches for lines including "BGC".
    When an "BGC" is found it is checked if it is in the clustername
    list already and else it gets added to the list. Also, the value
    corresponding to the selected Modus is added to the Value
    Dictionary, with a combination of the BGCs in the line as a key. 
    The sorted clustername list and a Value Dictionary is returned.
    """
    print("Extracting Data")
    start_time = time.time()
    InputNetwork = open(InFileName, "r")
    Modi = {
        "Raw": 2,
        "Jaccard" : 4,
        "DSS" : 5,
        "Adjacency" : 6
    }
    Clusters = []
    ValueDict = {}
    for line in InputNetwork:
        line = line.split()
        if line[0].startswith("BGC"):
            if line[0] in Clusters:
                ValueDict[line[0] + " + " + line[1]] = line[Modi[Modus]]
            else:
                Clusters.append(line[0])
                ValueDict[line[0] + " + " + line[1]] = line[Modi[Modus]]
        else:
            pass
        if line[1].startswith("BGC"):
            if line[1] in Clusters:
                pass
            else:
                Clusters.append(line[1])
        else:
            pass
    InputNetwork.close()
    elapsed_time = time.time() - start_time
    print("Done Extracting Data    " + str(elapsed_time))
    return(sorted(Clusters), ValueDict)


def Create_Cluster_NDArray(Modus, ClusternameList, ValueDict):
    """
    First, this function creates a numpy NDArray, filled with either
    0.00s or 1.00s, with the dimension based on the number of BGCs in
    the ClusternameList. Then it checks for the presence of all possible
    combinations of BGCs in the Value Dictionary. If an entry exists,
    its value is filled in at the appropriate spot in the NDArray.
    The function also fills in either a 0.00 or 1.00 at the spots in the
    NDArray, where the cluster on both axes is identical. The function
    returns a filled in NDArray of all the input data.
    """
    print("Creating NDArray")
    start_time = time.time()
    if Modus == "Raw":
        ClusterNDArray = np.ones((len(ClusternameList), len(ClusternameList)), dtype=float, order=None)
        for cluster_1 in ClusternameList:
            for cluster_2 in ClusternameList[ClusternameList.index(cluster_1):]:
                if (cluster_1 + " + " + cluster_2) in ValueDict:
                    ClusterNDArray[ClusternameList.index(cluster_1)][ClusternameList.index(cluster_2)] = ValueDict[cluster_1 + " + " + cluster_2]
                    ClusterNDArray[ClusternameList.index(cluster_2)][ClusternameList.index(cluster_1)] = ValueDict[cluster_1 + " + " + cluster_2]
                elif cluster_1 == cluster_2:
                    ClusterNDArray[ClusternameList.index(cluster_1)][ClusternameList.index(cluster_2)] = 0.00
                else:
                    pass
    else:
        ClusterNDArray = np.zeros((len(ClusternameList), len(ClusternameList)), dtype=float, order=None)
        for cluster_1 in ClusternameList:
            for cluster_2 in ClusternameList[ClusternameList.index(cluster_1):]:
                if (cluster_1 + " + " + cluster_2) in ValueDict:
                    ClusterNDArray[ClusternameList.index(cluster_1)][ClusternameList.index(cluster_2)] = ValueDict[cluster_1 + " + " + cluster_2]
                    ClusterNDArray[ClusternameList.index(cluster_2)][ClusternameList.index(cluster_1)] = ValueDict[cluster_1 + " + " + cluster_2]
                elif cluster_1 == cluster_2:
                    ClusterNDArray[ClusternameList.index(cluster_1)][ClusternameList.index(cluster_2)] = 1.00
                else:
                    pass
    elapsed_time = time.time() - start_time
    print("Done Creating NDArray    " + str(elapsed_time))
    return(ClusterNDArray)

def Write_OutFile(Modus, ClusternameList, ClusterNDArray, OutFileName):
    """
    This function writes an outfile named by the OutFileName input.
    inside the outfile, it writes the modus used, followed by all the
    cluster names, tab-saparated, followed by a line break.
    Then, for every cluster, the function writes down the clustername,
    followed by a tab and the corresponding values in the NDArray, also
    tab-separated.
    The function returns the completed outfile.
    """
    print("Writing " + OutFileName)
    start_time = time.time()
    OutFile = open(OutFileName, "w")
    OutFile.write(Modus + "\t")
    for cluster in ClusternameList:
        OutFile.write(cluster + "\t")
    OutFile.write("\n")
    for cluster in ClusternameList:
        OutFile.write(cluster + "\t")
        for value in ClusterNDArray[ClusternameList.index(cluster)]:
            OutFile.write(str(value) + "\t")
        OutFile.write("\n")
    OutFile.close()
    elapsed_time = time.time() - start_time
    print("Done Writing " + OutFileName + "    " + str(elapsed_time))
    return(OutFile)
    
if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    begin_time = time.time()
    In_File_Name = argv[1]
    Extraction_Modus = argv[2]
    Extraction_Modi = ["Raw", "Jaccard", "DSS", "Adjacency"]
    if not Extraction_Modus in Extraction_Modi:
        print("Please choose a viable Extraction Modus:" + "\n" + "Raw, Jaccard, DSS, or Adjacency")
        exit()
    else:
        pass
    Out_File_Name = In_File_Name.replace(".network", "_matrix_" + Extraction_Modus + ".txt")
    #print(Out_File_Name)
    Data = Get_Data(In_File_Name, Extraction_Modus)
    Clustername_List, Value_Dict = Data
    #print(Clustername_List)
    #print(Value_Dict)
    Cluster_NDArray = Create_Cluster_NDArray(Extraction_Modus, Clustername_List, Value_Dict)
    #print(ClusterNDArray)
    Out_File = Write_OutFile(Extraction_Modus, Clustername_List, Cluster_NDArray, Out_File_Name)
    end_time = time.time()
    run_duration = end_time-begin_time
    print("Finished Network Matrix" + "\n" + "Run Duration: " + str(run_duration))
