#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

A script that uses Biopython's SeqIO, to parse the .gbk-files in a
folder and fill a dictionary with the BGC-name as key and the annotation
as value. Afterwards, the dictionary entries are, one by one, written
into a tab-delimited text file.
"""

from sys import argv
import os
from Bio import SeqIO

def Parse_GBK(GBK_Folder):
    """
    This function goes through all the GBK-files in a folder. It
    extracts the BGC-name and the respective annotation from the file.
    It creates an Annotation Dictionary from this information, with the
    BGC-names as keys and the annotations as values.
    """
    AnnotationDict = {}
    n = 0
    annotation = ""
    for GBK in sorted(os.listdir(GBK_Folder)):
        n = 0
        if GBK.endswith("0000001.gbk"):
            record = SeqIO.read(GBK, "genbank")
            for feature in record.features:
                if feature.type == "CDS":
                    n += 1
                    annotation = feature.qualifiers.get("product")
                    AnnotationDict[record.id + "_" + feature.type + "_" + str(n)] = annotation
                    annotation = ""
                else:
                    pass
        else:
            pass
    return(AnnotationDict)


def Write_OutFile(OutFileName, AnnotationDict):
    OutFile = open(OutFileName, "w")
    for entry in AnnotationDict.items():
        key, value = entry
        OutFile.write("{}\t{}\n".format(key, value))
    OutFile.close()
    return(OutFile)


if __name__ == "__main__":
    GBK_Directory_Input = argv[1]
    print(GBK_Directory_Input)
    GBK_Directory_Path = os.path.realpath(GBK_Directory_Input)
    print(GBK_Directory_Path)
    GBK_Directory_Name = os.path.basename(GBK_Directory_Path)
    print(GBK_Directory_Name)
    Out_File_Name = GBK_Directory_Name + "_Annotations.txt"
    print(Out_File_Name)
    Annotation_Dict = Parse_GBK(GBK_Directory_Path)
    Out_File = Write_OutFile(Out_File_Name, Annotation_Dict)
