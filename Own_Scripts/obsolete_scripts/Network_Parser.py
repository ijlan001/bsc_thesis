#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

FILLER TEXT
"""

# import statements go here
from sys import argv
import subprocess
import os.path

def Get_Clusternames(InputNetwork):
    print("Creating Cluster List")
    Clusters = []
    for line in InputNetwork:
        line = line.split()
        if not line[0].startswith("BGC"):
            pass
        else:
            if line[0] in Clusters:
                pass
            else:
                Clusters.append(line[0])
        if not line[1].startswith("BGC"):
            pass
        else:
            if line[1] in Clusters:
                pass
            else:
                Clusters.append(line[1])
    print("Done Creating Cluster List")
    return(sorted(Clusters))

def Create_Distance_List(InputNetwork, ClusterList):
    print("Creating Distance List")
    tmp = ()
    DistanceList = []
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            if cluster_1 == cluster_2:
                tmp = (cluster_1, cluster_2, str(0.00))
                DistanceList.append(tmp)
                tmp = ()
            else:
                InputNetwork.seek(0)
                for line in InputNetwork:
                    line = line.split()
                    if (cluster_1, cluster_2) == (line[0], line[1]):
                        tmp = (cluster_1, cluster_2, line[2])
                        DistanceList.append(tmp)
                        tmp = ()
                    else:
                        pass
    print("Done Creating Distance List")
    return(DistanceList)

def Fill_Blanks(ClusterList, DistanceList):
    print("Completing Distance List")
    CompleteList = DistanceList
    present = False
    tmp = ()
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            for entry in CompleteList:
                name_1, name_2, raw = entry
                if (name_1, name_2) == (cluster_1, cluster_2):
                    present = True
                else:
                    if (name_2, name_1) == (cluster_1, cluster_2):
                        present = True
                    else:
                        pass
            if present == False:
                tmp = (cluster_1, cluster_2, str(1.00))
                CompleteList.append(tmp)
            else:
                present = False
                tmp = ()
    tmp = []
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            if cluster_1 == cluster_2:
                pass
            else:
                for entry in CompleteList:
                    name_1, name_2, raw = entry
                    if (cluster_1, cluster_2) == (name_1, name_2):
                        tmp.append((cluster_2, cluster_1, raw))
    nr = -1
    for entry in tmp:
        nr += 1
        CompleteList.append(tmp[nr])
    print("Done Completing Distance List")
    return(sorted(CompleteList))

def Create_Matrix(ClusterList, CompleteList, OutFileName):
    print("Creating Matrix File")
    OutFile = open(OutFileName, "w")
    OutFile.write("Matrix" + "\t")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
    OutFile.write("\n")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
        for entry in CompleteList:
            name_1, name_2, raw = entry
            if name_1 == cluster:
                OutFile.write(raw + "\t")
            else:
                pass
        OutFile.write("\n")
    print("Finished Creating Matrix File")
    OutFile.close()


if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    Input_File = argv[1]
    Output_File = argv[2]
    Clusters_List = Get_Clusternames(open(Input_File, "r"))
    Distance_List = Create_Distance_List(open(Input_File, "r"), Clusters_List)
    Complete_List = Fill_Blanks(Clusters_List, Distance_List)
    print(Complete_List)
    Create_Matrix(Clusters_List, Complete_List, Output_File)
