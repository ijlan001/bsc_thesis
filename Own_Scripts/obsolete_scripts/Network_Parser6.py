#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

A script that creates a tab-delimited with a dissociation matrix.
First it creates a list of BGCs that where found in the network-file.
Then it creates a list of tuples, that all exist out of three values:
the first BGC's name, the second BGC's name and the Raw value.
Afterwards it fills in the rest of the list with the BGC-combinations
that are not included in the list yet.
Then, in the first row of the output-file, it prints the names of all
the BGCs.
The following lines are filled with the name of a BGC in the first
column, followed by all the respective raw dissociation values.
"""

# import statements go here
from sys import argv
import subprocess
import os.path
import time
import multiprocessing as mp
print("Number of processors: ", mp.cpu_count())

def Get_Clusternames(InputNetwork):
    """Parses the input-file and searches for names including BGC.
    When an BGC is found it is checked if it is in the clustername list
    already and else it gets added to the list.
    The sorted clustername list is returned.
    """
    print("Creating Cluster List")
    start_time = time.time()
    Clusters = []
    for line in InputNetwork:
        line = line.split()
        if line[0].startswith("BGC"):
            if line[0] in Clusters:
                pass
            else:
                Clusters.append(line[0])
        else:
            pass
        if line[1].startswith("BGC"):
            if line[1] in Clusters:
                pass
            else:
                Clusters.append(line[1])
        else:
            pass
    elapsed_time = time.time() - start_time
    print("Done Creating Cluster List    " + str(elapsed_time))
    return(sorted(Clusters))

def Create_Distance_List(InputNetwork, ClusterList):
    """This function takes the input-file and the clustername list as
    input. The function iterates through the list of Cluster names and
    adds the entries of a cluster that is compared to itself to the
    Distance list, the list of clustercombinations and their distance.
    Afterwards, the function goes through the input-file and adds all
    the entries in the file to the Distance list.
    The sorted distance list is returned.
    """
    print("Creating Distance List")
    start_time = time.time()
    tmp = ()
    tmp_1 = ()
    tmp_2 = ()
    DistanceList = []
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            if cluster_1 == cluster_2:
                tmp = (cluster_1, cluster_2, str(0.00))
                DistanceList.append(tmp)
                tmp = ()
            else:
                pass
    for line in InputNetwork:
        line = line.split()
        if line[0] == "Clustername":
            pass
        else:
            tmp_1 = (line[0], line[1], line[2])
            tmp_2 = (line[1], line[0], line[2])
            DistanceList.append(tmp_1)
            DistanceList.append(tmp_2)
            tmp_1 = ()
            tmp_2 = ()
    elapsed_time = time.time() - start_time
    print("Done Creating Distance List    " + str(elapsed_time))
    return(sorted(DistanceList))

def Fill_Blanks(cluster_1, ClusterList, DistanceList):
    """The function takes a cluster in the clustername list, the
    clustername list and the distance list as input.
    The function iterates through the clustername list, from the point
    of the cluster given as input. The function checks if the
    combination of clusters is already present in the distance list.
    If it is true, the function continues. If it is false, the function
    adds the clustercombination to the return_dict dictionary. 
    """
    print("Fill Blanks    " + cluster_1)
    start_time = time.time()
    FillList = []
    present = False
    tmp_1 = ()
    tmp_2 = ()
    for cluster_2 in ClusterList[ClusterList.index(cluster_1):]:
        for entry in DistanceList:
            name_1, name_2, raw = entry
            if (name_1, name_2) == (cluster_1, cluster_2):
                present = True
            else:
                pass
        if present == False:
            tmp_1 = (cluster_1, cluster_2, str(1.00))
            tmp_2 = (cluster_2, cluster_1, str(1.00))
            FillList.append(tmp_1)
            FillList.append(tmp_2)
            tmp_1 = ()
            tmp_2 = ()
        else:
            present = False
            tmp_1 = ()
            tmp_2 = ()
    return_dict[cluster_1] = FillList
    elapsed_time = time.time() - start_time
    print("Done Filling Blanks    " + cluster_1 + "    " + str(elapsed_time))
    return

def Create_Matrix(ClusterList, CompleteList, OutFileName):
    """
    This function takes the Clustername list, the Completed Distance
    list and the outfile name as input. The function first opens the
    outfile to write. The function writes down "Matrix" followed by
    a tab and all clusters in the clustername list, tab saparated.
    Then the function writes down the first clustername and the sorted
    raw entries of the completed distance list coresponding to the
    specific cluster, followed by a line break. This process is repeated
    for all the entries in the Completed Distance list and finaly the
    outfile is closed.
    """
    print("Creating Matrix File")
    start_time = time.time()
    OutFile = open(OutFileName, "w")
    OutFile.write("Matrix" + "\t")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
    OutFile.write("\n")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
        for entry in CompleteList:
            name_1, name_2, raw = entry
            if name_1 == cluster:
                OutFile.write(raw + "\t")
            else:
                pass
        OutFile.write("\n")
    elapsed_time = time.time() - start_time
    print("Finished Creating Matrix File    " + str(elapsed_time))
    OutFile.close()
    
    
if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    begin_time = time.time()
    Input_File = argv[1]
    Output_File = argv[2]
    Clusters_List = Get_Clusternames(open(Input_File, "r"))
    Distance_List = Create_Distance_List(open(Input_File, "r"), Clusters_List)
    manager = mp.Manager()
    return_dict = manager.dict()
    processes = []
    for cluster in Clusters_List:
        p = mp.Process(target=Fill_Blanks, args=(cluster, Clusters_List, Distance_List))
        processes.append(p)
        p.start()
    for process in processes:
        process.join()
    Fill_List = return_dict.values()
    Complete_List = Distance_List
    for entry in Fill_List:
        Complete_List += entry
    Complete_List = sorted(Complete_List)
    Create_Matrix(Clusters_List, Complete_List, Output_File)
    total_time = time.time() - begin_time
    print("Finished in:    " + str(total_time))
