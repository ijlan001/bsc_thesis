#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

A script that creates a tab-delimited with a dissociation matrix.
First it creates a list of BGCs that where found in the network-file.
Then it creates a list of tuples, that all exist out of three values:
the first BGC's name, the second BGC's name and the Raw value.
Afterwards it fills in the rest of the list with the BGC-combinations
that are not included in the list yet.
Then, in the first row of the output-file, it prints the names of all
the BGCs.
The following lines are filled with the name of a BGC in the first
column, followed by all the respective raw dissociation values.

"""

# import statements go here
from sys import argv
import subprocess
import os.path
import time
import multiprocessing as mp
print("Number of processors: ", mp.cpu_count())

def Get_Clusternames(InputNetwork):
    print("Creating Cluster List")
    start_time = time.time()
    Clusters = []
    for line in InputNetwork:
        line = line.split()
        if line[0].startswith("BGC"):
            if line[0] in Clusters:
                pass
            else:
                Clusters.append(line[0])
        else:
            pass
        if line[1].startswith("BGC"):
            if line[1] in Clusters:
                pass
            else:
                Clusters.append(line[1])
        else:
            pass
    elapsed_time = time.time() - start_time
    print("Done Creating Cluster List    " + str(elapsed_time))
    return(sorted(Clusters))

def Create_Distance_List(InputNetwork, ClusterList):
    print("Creating Distance List")
    start_time = time.time()
    tmp_1 = ()
    tmp_2 = ()
    DistanceList = []
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            if cluster_1 == cluster_2:
                tmp = (cluster_1, cluster_2, str(0.00))
                DistanceList.append(tmp)
                tmp = ()
            else:
                pass
    for line in InputNetwork:
        line = line.split()
        if line[0] == "Clustername":
            pass
        else:
            tmp_1 = (line[0], line[1], line[2])
            tmp_2 = (line[1], line[0], line[2])
            DistanceList.append(tmp_1)
            DistanceList.append(tmp_2)
            tmp_1 = ()
            tmp_2 = ()
    elapsed_time = time.time() - start_time
    print("Done Creating Distance List    " + str(elapsed_time))
    return(DistanceList)

def Fill_Blanks(cluster_1, ClusterList, DistanceList):
    print("Fill Blanks    " + cluster_1)
    start_time = time.time()
    CompleteList = []
    present = False
    tmp_1 = ()
    tmp_2 = ()
    for cluster_2 in ClusterList:
        for entry in DistanceList:
            name_1, name_2, raw = entry
            if (name_1, name_2) == (cluster_1, cluster_2):
                present = True
            else:
                pass
        if present == False:
            tmp = (cluster_1, cluster_2, str(1.00))
            CompleteList.append(tmp)
            tmp = ()
        else:
            present = False
            tmp = ()
    elapsed_time = time.time() - start_time
    print("Done Filling Blanks    " + cluster_1 + "    " + str(elapsed_time))
    return(sorted(CompleteList))

def Create_Matrix(ClusterList, CompleteList, OutFileName):
    print("Creating Matrix File")
    start_time = time.time()
    OutFile = open(OutFileName, "w")
    OutFile.write("Matrix" + "\t")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
    OutFile.write("\n")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
        for entry in CompleteList:
            name_1, name_2, raw = entry
            if name_1 == cluster:
                OutFile.write(raw + "\t")
            else:
                pass
        OutFile.write("\n")
    elapsed_time = time.time() - start_time
    print("Finished Creating Matrix File    " + str(elapsed_time))
    OutFile.close()


if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    Input_File = argv[1]
    Output_File = argv[2]
    pool = mp.Pool(mp.cpu_count())
    Clusters_List = Get_Clusternames(open(Input_File, "r"))
    Distance_List = Create_Distance_List(open(Input_File, "r"), Clusters_List)
    Complete_List = [pool.apply(Fill_Blanks, args=(cluster, Clusters_List, Distance_List)) for cluster in Clusters_List]
    pool.close()
    for x in Complete_List:
        for y in x:
            Distance_List.append(y)
    print(Distance_List)
    Complete_List = sorted(Distance_List)
    print(Complete_List)
    Create_Matrix(Clusters_List, Complete_List, Output_File)
