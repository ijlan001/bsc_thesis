#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

FILLER TEXT
"""

# import statements go here
from sys import argv
import subprocess
import os.path
import time

def Get_Clusternames(InputNetwork):
    print("Creating Cluster List")
    start_time = time.time()
    Clusters = []
    for line in InputNetwork:
        line = line.split()
        if not line[0].startswith("BGC"):
            pass
        else:
            if line[0] in Clusters:
                pass
            else:
                Clusters.append(line[0])
        if not line[1].startswith("BGC"):
            pass
        else:
            if line[1] in Clusters:
                pass
            else:
                Clusters.append(line[1])
    elapsed_time = time.time() - start_time
    print("Done Creating Cluster List   " + str(elapsed_time))
    return(sorted(Clusters))

def Create_Distance_List(InputNetwork, ClusterList):
    print("Creating Distance List")
    start_time = time.time()
    tmp = ()
    DistanceList = []
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            if cluster_1 == cluster_2:
                tmp = (cluster_1, cluster_2, str(0.00))
                DistanceList.append(tmp)
                tmp = ()
            else:
                pass
    for line in InputNetwork:
        line = line.split()
        tmp = (line[0], line[1], line[2])
        DistanceList.append(tmp)
        tmp = ()
    elapsed_time = time.time() - start_time
    print("Done Creating Distance List  " + str(elapsed_time))
    return(DistanceList)

def Fill_Blanks(ClusterList, DistanceList):
    print("Completing Distance List")
    start_time = time.time()
    CompleteList = DistanceList
    present = False
    tmp = ()
    for cluster_1 in ClusterList:
        for cluster_2 in ClusterList:
            for entry in CompleteList:
                name_1, name_2, raw = entry
                if (name_1, name_2) == (cluster_1, cluster_2):
                    present = True
                else:
                    if (name_2, name_1) == (cluster_1, cluster_2):
                        present = True
                    else:
                        pass
            if present == False:
                tmp = (cluster_1, cluster_2, str(1.00))
                CompleteList.append(tmp)
            else:
                present = False
                tmp = ()
    tmp = []
    for entry in CompleteList:
        name_1, name_2, raw = entry
        if name_1 == name_2:
            pass
        else:
            tmp.append((name_2, name_1, raw))
    nr = -1
    for entry in tmp:
        nr += 1
        CompleteList.append(tmp[nr])
    elapsed_time = time.time() - start_time
    print("Done Completing Distance List    " + str(elapsed_time))
    return(sorted(CompleteList))

def Create_Matrix(ClusterList, CompleteList, OutFileName):
    print("Creating Matrix File")
    start_time = time.time()
    OutFile = open(OutFileName, "w")
    OutFile.write("Matrix" + "\t")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
    OutFile.write("\n")
    for cluster in ClusterList:
        OutFile.write(cluster + "\t")
        for entry in CompleteList:
            name_1, name_2, raw = entry
            if name_1 == cluster:
                OutFile.write(raw + "\t")
            else:
                pass
        OutFile.write("\n")
    elapsed_time = time.time() - start_time
    print("Finished Creating Matrix File    " + str(elapsed_time))
    OutFile.close()


if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    Input_File = argv[1]
    Output_File = argv[2]
    Clusters_List = Get_Clusternames(open(Input_File, "r"))
    Distance_List = Create_Distance_List(open(Input_File, "r"), Clusters_List)
    Complete_List = Fill_Blanks(Clusters_List, Distance_List)
    Create_Matrix(Clusters_List, Complete_List, Output_File)
