#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

creates a neater list of BGC Annotation.
"""

# import statements go here
from sys import argv
from itertools import chain
from collections import OrderedDict
import subprocess
import os.path

def Obtain_GBKs(GBKFolder):
    GBKList = []
    for GBK in sorted(os.listdir(GBKFolder)):
        if GBK.endswith(".gbk"):
            GBK = GBK[:-4]
            GBK = GBK + ".1"
            GBKList.append(GBK)
        else:
            pass
    return(GBKList)

def Obtain_Raw_Annotation(InFileName):
    RawAnnotationList = []
    InFile = open(InFileName, "r")
    for line in InFile:
        line = line.split("\t")
        line[1] = line[1].split("\n")[0]
        RawAnnotationList.append((line[0],line[1]))
    return(sorted(RawAnnotationList))


def Select_Annotation(RawAnnotationList, GBKList):
    NewAnnotationList = []
    BGCAnnotationList = []
    DuplicateList = []
    TempList = []
    for entry in RawAnnotationList:
        BGC, Compound = entry
        BGCAnnotationList.append(BGC)
        
    for GBK in GBKList:
        if BGCAnnotationList.count(GBK) == 0:
            NewAnnotationList.append((GBK, "Unknown"))
        elif BGCAnnotationList.count(GBK) > 1:
            indices = [x[1] for (i, x) in enumerate(RawAnnotationList) if x[0] == GBK]
            for name in indices:
                name = name.split(" ")
                TempList.append(name[0])
            #for name in TempList:
            #    if TempList.count(name) > 1:
            #        NewAnnotationList.append((GBK, name))
            #    else:
            #        print("{} has multiple annotations:".format(GBK))
            #        for name in TempList:
            #            print(name)
            #        NewAnnotationList.append((GBK, ))
            #    TempList = []
            #    break
            FusedAnnotation = ""
            for i in set(TempList):
                FusedAnnotation += i + "/"
            FusedAnnotation = FusedAnnotation[:-1]
            NewAnnotationList.append((GBK, FusedAnnotation))
        else:
            NewAnnotationList.append((GBK, [x[1] for (i, x) in enumerate(RawAnnotationList) if x[0] == GBK][0]))
    return(sorted(NewAnnotationList))


def Write_OutFile(NewAnnotationList, OutFileName):
    OutFile = open(OutFileName, "w")
    OutFile.write("BGC\tAnnotation\n")
    for entry in NewAnnotationList:
        BGC, Annotation = entry
        OutFile.write("{}\t{}\n".format(BGC[:-2], str(Annotation)))
    OutFile.close()


if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    In_File_Name = argv[1]
    GBK_Directory_Input = argv[2]
    GBK_Directory_Path = os.path.realpath(GBK_Directory_Input)
    GBK_Directory_Name = os.path.basename(GBK_Directory_Path)
    Out_File_Name = "New_" + GBK_Directory_Name + "_" + In_File_Name
    
    GBK_List = Obtain_GBKs(GBK_Directory_Path)
    Raw_Annotation_List = Obtain_Raw_Annotation(In_File_Name)
    New_Annotation_List = Select_Annotation(Raw_Annotation_List, GBK_List)
    Out_File = Write_OutFile(New_Annotation_List, Out_File_Name)
