#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

FILLER TEXT
"""

# import statements go here
from sys import argv
import subprocess
import os.path

def obtain_domains(text_file):
    
    domain_list = []
    code_list = []
    for line in text_file:
        line = line.split()
        if any(x == "included" for x in line):
            domain = line[0]
            name = line[1]
            domain_list += [(line[0], line[1])]
            code_list += [line[0]]
        else:
            continue
    return(domain_list, code_list)


def create_New_HMM(codes, FULL_HMM):
    tmp = ""
    filtered_HMM = ""
    for line in FULL_HMM:
        if line.startswith("ACC"):
            tmp += line
            line = line.split()
            if line[1] in codes:
                included = True
            else:
                included = False
        elif line.startswith("//"):
            tmp += line
            if included == True:
                filtered_HMM += tmp
            tmp = ""
        else:
            tmp += line
    return(filtered_HMM)


def write_HMM(filtered_HMM, OutFileName):
    OutFile = open(OutFileName, "w")
    OutFile.write(filtered_HMM)
    OutFile.close()


if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    InputText = argv[1]
    Full_HMM = argv[2]
    HMM_out = argv[3]
    relevant_domains, domain_codes = obtain_domains(open(InputText, "r"))
    New_HMM = create_New_HMM(domain_codes, open(Full_HMM, "r"))
    write_HMM(New_HMM, HMM_out)
