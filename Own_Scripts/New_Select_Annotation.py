#!/usr/bin/env python3

"""
Author: Erik IJland
Studentnr.: 960504980110

creates a neater list of BGC Annotation.
"""

# import statements go here
from sys import argv
from itertools import chain
from collections import OrderedDict
import subprocess
import os.path

def Obtain_GBKs(GBKFolder):
    GBKList = []
    for GBK in sorted(os.listdir(GBKFolder)):
        if GBK.endswith(".gbk"):
            GBK = GBK[:-4]
            GBK = GBK + ".1"
            GBKList.append(GBK)
        else:
            pass
    return(sorted(GBKList))


def Obtain_Raw_BGC_Products(InFileName):
    RawProductList = []
    InFile = open(InFileName, "r")
    for line in InFile:
        line = line.split("\t")
        line[1] = line[1].split("\n")[0]
        RawProductList.append((line[0],line[1]))
    return(sorted(RawProductList))


def Obtain_Raw_Compound_Groups(InFileName):
    RawCompoundGroupList = []
    InFile = open(InFileName, "r")
    for line in InFile:
        if line.startswith("Class"):
            pass
        else:
            line = line.split("\t")
            line[3] = line[3].split("\n")[0]
            RawCompoundGroupList.append((line[3], line[2]))
    return(sorted(RawCompoundGroupList))


def Combine_Product_Groups(RawProductList, RawGroupList):
    RawAnnotationList = []
    for entry_1 in RawProductList:
        BGC, Product = entry_1
        for entry_2 in RawGroupList:
            Compound, Group = entry_2
            if Product == Compound:
                if (BGC, Group) in RawAnnotationList:
                    pass
                else:
                    RawAnnotationList.append((BGC, Group))
            else:
                pass
    return(sorted(RawAnnotationList))


def Form_New_Annotation(GBKList, RawAnnotationList):
    NewAnnotationList = []
    IncludedList = []
    for GBK in GBKList:
        for entry in RawAnnotationList:
            BGC, Group = entry
            if GBK == BGC:
                NewAnnotationList.append((GBK, Group))
                IncludedList.append(GBK)
            else:
                pass
    for GBK in GBKList:
        if GBK in IncludedList:
            pass
        else:
            NewAnnotationList.append((GBK, "Unknown"))
    return(sorted(NewAnnotationList))


def Get_Extra_Information(NewAnnotationList):
    GroupCountDict = {}
    BGCCountDict = {}
    for entry in NewAnnotationList:
        BGC, Group = entry
        if Group in GroupCountDict.keys():
            GroupCountDict[Group] += 1
        else:
            GroupCountDict[Group] = 1
        if BGC in BGCCountDict.keys():
            BGCCountDict[BGC] += 1
        else:
            BGCCountDict[BGC] = 1
    return(GroupCountDict, BGCCountDict)

def Write_OutFile_1(NewAnnotationList, OutFileName):
    OutFile = open(OutFileName, "w")
    OutFile.write("BGC\tGroup\n")
    for entry in NewAnnotationList:
        BGC, Group = entry
        OutFile.write("{}\t{}\n".format(BGC[:-2], Group))
    OutFile.close()
    return(OutFile)


def Write_OutFile_2(ExtraInformation, OutFileName, NewAnnotationList):
    GroupCountDict, BGCCountDict = ExtraInformation
    OutFile = open(OutFileName, "w")
    OutFile.write("Group\tNumber_Of_BGCs\n")
    for entry in GroupCountDict.items():
        Group, Number = entry
        OutFile.write("{}\t{}\n".format(Group, Number))
    OutFile.write("\nBGC_With_>1_Groups\tNumber_Of_Groups\tGroups\n")
    for entry_1 in BGCCountDict.items():
        BGC, Number = entry_1
        tmp = ""
        if Number > 1:
            for entry_2 in NewAnnotationList:
                GBK, Group = entry_2
                if BGC == GBK:
                    tmp += Group + "/"
                else:
                    pass
            OutFile.write("{}\t{}\n".format(BGC, tmp))
        else:
            pass
    OutFile.close()
    return(OutFile)


if __name__ == "__main__":
    # code which is executed when the script is called
    #from the command line, goes here
    In_File_Name_1 = argv[1]
    In_File_Name_2 = argv[2]
    GBK_Directory_Input = argv[3]
    GBK_Directory_Path = os.path.realpath(GBK_Directory_Input)
    GBK_Directory_Name = os.path.basename(GBK_Directory_Path)
    Out_File_Name_1 = "New_" + GBK_Directory_Name + "_Annotation.txt"
    Out_File_Name_2 = "New_" + GBK_Directory_Name + "_Annotation_Extra_Information.txt"
    
    GBK_List = Obtain_GBKs(GBK_Directory_Path)
    Raw_Product_List = Obtain_Raw_BGC_Products(In_File_Name_1)
    Raw_Compound_Group_List = Obtain_Raw_Compound_Groups(In_File_Name_2)
    Raw_Annotation_List = Combine_Product_Groups(Raw_Product_List, Raw_Compound_Group_List)
    New_Annotation_List = Form_New_Annotation(GBK_List, Raw_Annotation_List)
    Extra_Information = Get_Extra_Information(New_Annotation_List)
    Out_File_1 = Write_OutFile_1(New_Annotation_List, Out_File_Name_1)
    Out_File_2 = Write_OutFile_2(Extra_Information, Out_File_Name_2, New_Annotation_List)
    
